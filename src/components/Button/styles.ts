import styled from 'styled-components';
import { shade } from 'polished';

export const ButtonField = styled.button`
  /* display: flex; */
  align-items: center;
  justify-content: center;
  background: #10ac84;
  color: #ffffff;
  font-size: 1.3em;
  font-weight: 700;
  border-radius: 4px;
  border: 0;
  padding: 16px;
  width: 100%;
  margin-top: 16px;
  transition: background-color 0.2s;

  svg {
    margin-right: 6px;
  }

  &:hover {
    background: ${shade(0.2, '#10ac84')};
  }
`;
