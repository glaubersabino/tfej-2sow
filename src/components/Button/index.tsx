import React, { ButtonHTMLAttributes } from 'react';

import { ButtonField } from './styles';

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

const Button: React.FC<ButtonProps> = ({ children, ...rest }) => (
  <ButtonField type="button" {...rest}>
    {children}
  </ButtonField>
);

export default Button;
