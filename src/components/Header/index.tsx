import React from 'react';
import { Link } from 'react-router-dom';
import { FiUsers, FiLogOut } from 'react-icons/fi';

import logoHorizontal from '../../assets/logo-horizontal.png';

import { HeaderContainer, HeaderMenu } from './styles';

const Header: React.FC = () => (
  <HeaderContainer>
    <img src={logoHorizontal} alt="UserFlex" />
    <HeaderMenu>
      <li>
        <Link to="/users">
          <FiUsers />
          Usuários
        </Link>
      </li>
      <li>
        <Link to="/logoff">
          <FiLogOut />
          Sair
        </Link>
      </li>
    </HeaderMenu>
  </HeaderContainer>
);

export default Header;
