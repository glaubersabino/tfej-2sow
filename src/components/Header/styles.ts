import styled from 'styled-components';

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 47px 0px 52px;

  img {
    max-width: 252px;
  }

  @media screen and (max-width: 767px) {
    flex-direction: column;
    margin: 27px 0px 32px;

    img {
      margin-bottom: 15px;
    }
  }
`;

export const HeaderMenu = styled.ul`
  display: flex;
  list-style: none;

  li + li {
    margin-left: 20px;
  }

  a {
    display: flex;
    align-items: center;
    font-size: 24px;
    font-weight: 600;
    color: #222f3e;
    text-decoration: none;
    transition: color 0.2s;

    svg {
      margin-right: 6px;
    }

    &:hover {
      color: #10ac84;
    }
  }
`;
