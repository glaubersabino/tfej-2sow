import React, { InputHTMLAttributes, useEffect, useRef } from 'react';
import InputMask, { Props as InputMaskProps } from 'react-input-mask';
import { useField } from '@unform/core';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

interface MaskProps extends InputHTMLAttributes<InputMaskProps> {
  mask?: string | Array<string | RegExp>;
}

type Props = InputProps & MaskProps;

const Input: React.FC<Props> = ({ name, mask, ...rest }) => {
  const inputRef = useRef(null);
  const { fieldName, defaultValue, error, registerField } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <>
      <InputMask mask={mask || ''} ref={inputRef} {...rest} />
      {error ? <p>{error}</p> : ''}
    </>
  );
};

export default Input;
