import React, { useCallback, useRef } from 'react';
import { FiUserPlus } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import * as Yup from 'yup';

import viacep from '../../services/viacep';
import getValidationErrors from '../../utils/getValidationErrors';

import Header from '../../components/Header';
import Input from '../../components/Input2';
import Button from '../../components/Button';

import { Container, Content } from './styles';

const Signup: React.FC = () => {
  const formRef = useRef<FormHandles>(null);

  const handleCEP = async (event: string): Promise<void> => {
    const cep = event.replace('-', '');
    console.log(cep);
    const response = await viacep.get(`${cep}/json/`);
    const { logradouro, bairro, localidade } = response.data;

    formRef.current?.setFieldValue('rua', logradouro);
    formRef.current?.setFieldValue('bairro', bairro);
    formRef.current?.setFieldValue('cidade', localidade);
  };

  const handleSubmit = useCallback(async (data: object) => {
    try {
      const schema = Yup.object().shape({
        name: Yup.string()
          .required('O nome é obrigatório.')
          .notOneOf(['admin'], 'Nome reservado para o sistema.'),
        cpf: Yup.string()
          .required('O CPF é obrigatório.')
          .matches(/[0-9]{3}.[0-9]{3}.[0-9]{3}-[\d]{2}/, 'CPF inválido.'),
        email: Yup.string()
          .required('O E-mail é obrigatório.')
          .email('E-mail inválido.'),
        cep: Yup.string()
          .required('O CEP é obrigatório.')
          .matches(/[0-9]{5}-[\d]{3}/, 'CEP inválido.'),
        rua: Yup.string().required('A rua é obrigatória.'),
        numero: Yup.number()
          .typeError('O valor deve ser númerico.')
          .positive('O número deve ser maior que 1.')
          .required('O número é obrigatório.'),
        bairro: Yup.string().required('O bairro é obrigatório.'),
        cidade: Yup.string().required('A cidade é obrigatória.'),
      });

      await schema.validate(data, { abortEarly: false });
    } catch (err) {
      console.log(err);

      const errors = getValidationErrors(err);

      formRef.current?.setErrors(errors);
    }
  }, []);

  return (
    <Container>
      <Header />
      <Content>
        <h1>
          <FiUserPlus />
          Cadastro de Usuários
        </h1>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <div className="tree-one">
            <label>
              Nome
              <Input
                type="text"
                name="name"
                mask=""
                placeholder="Insira seu Nome"
              />
            </label>
            <label>
              CPF
              <Input
                type="text"
                name="cpf"
                mask="999.999.999-99"
                placeholder="Insira seu CPF"
              />
            </label>
          </div>
          <div className="tree-one">
            <label>
              E-mail
              <Input
                type="email"
                name="email"
                mask=""
                placeholder="Insira seu E-mail"
              />
            </label>
            <label>
              CEP
              <Input
                type="text"
                name="cep"
                mask="99999-999"
                onBlur={event => handleCEP(event.target.value)}
                placeholder="Insira seu CEP"
              />
            </label>
          </div>
          <div className="tree-one">
            <label>
              Rua
              <Input
                type="text"
                name="rua"
                mask=""
                placeholder="Insira sua Rua"
              />
            </label>
            <label>
              Número
              <Input
                type="number"
                name="numero"
                mask=""
                placeholder="Insira seu Número"
              />
            </label>
          </div>
          <div className="two-two">
            <label>
              Bairro
              <Input
                type="text"
                name="bairro"
                mask=""
                placeholder="Insira seu Bairro"
              />
            </label>
            <label>
              Cidade
              <Input
                type="text"
                name="cidade"
                mask=""
                placeholder="Insira sua Cidade"
              />
            </label>
          </div>

          <div className="actions">
            <Button type="submit">
              <FiUserPlus />
              Cadastrar Usuário
            </Button>
          </div>
        </Form>
      </Content>
    </Container>
  );
};

export default Signup;
