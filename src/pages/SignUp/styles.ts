import styled from 'styled-components';

export const Container = styled.div`
  width: 90%;
  margin: 0 auto;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    display: flex;
    align-items: center;
    font-size: 2.5em;

    svg {
      margin-right: 16px;
    }

    @media screen and (max-width: 767px) {
      font-size: 1.5em;

      svg {
        margin-right: 6px;
      }
    }
  }

  form {
    margin: 24px 0 80px 0;
    text-align: center;

    div {
      display: flex;
      flex-wrap: wrap;
      flex: 1;
      margin-bottom: 24px;

      text-align: left;

      label + label {
        margin-left: 36px;
        width: 100%;
      }

      input {
        margin-top: 3px;
        background: #ffffff;
        border-radius: 4px;
        border: 2px solid #222f3e;
        padding: 16px;
        width: 100%;

        & + input {
          margin-top: 33px;
        }

        &::placeholder {
          color: #222f3e;
        }

        &:focus {
          border: 2px solid #10ac84;
        }
      }

      p {
        margin-top: 3px;
        color: #ee5253;
      }

      &.tree-one {
        label:first-child {
          flex: 1;
        }

        label + label {
          flex-basis: 25%;
        }

        @media screen and (max-width: 767px) {
          label {
            margin-left: 0;
            flex-basis: 100% !important;
          }
        }
      }

      &.two-two {
        label {
          flex-basis: 47%;
          flex: 1;
        }

        @media screen and (max-width: 767px) {
          label {
            margin-left: 0;
            flex-basis: 100%;
          }
        }
      }

      &.actions {
        justify-content: center;

        button {
          display: flex;
          align-items: center;
          max-width: 273px;
        }
      }

      @media screen and (max-width: 767px) {
        label + label {
          margin-top: 24px;
        }
      }
    }
  }
`;
