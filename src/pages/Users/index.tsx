import React from 'react';
import { Link } from 'react-router-dom';

import {
  FiUsers,
  FiUserPlus,
  FiEdit,
  FiTrash2,
  FiRepeat,
} from 'react-icons/fi';

import Header from '../../components/Header';

import {
  Container,
  Content,
  ContentHeader,
  ContentHeaderMenu,
  ContentTable,
} from './styles';

const Users: React.FC = () => (
  <Container>
    <Header />
    <Content>
      <ContentHeader>
        <ContentHeaderMenu>
          <h1>
            <FiUsers />
            Usuários
          </h1>
          <Link to="/signup">
            <FiUserPlus />
            Novo Usuário
          </Link>
        </ContentHeaderMenu>
        <input type="text" name="search" placeholder="Pesquise aqui" />
      </ContentHeader>
      <ContentTable>
        <table>
          <thead>
            <tr>
              <th>Nome</th>
              <th>CPF</th>
              <th>E-mail</th>
              <th>Cidade</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
            <tr>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>John Lennon</td>
              <td>Rhythm Guitar</td>
              <td>
                <a href="https://react-icons.github.io/" className="edit">
                  <FiEdit />
                </a>
                <a href="https://react-icons.github.io/" className="remove">
                  <FiTrash2 />
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </ContentTable>
    </Content>
  </Container>
);

export default Users;
