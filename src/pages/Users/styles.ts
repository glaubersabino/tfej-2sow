import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.div`
  width: 90%;
  margin: 0 auto;
`;

export const Content = styled.div``;

export const ContentHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 28px;

  h1 {
    display: flex;
    align-items: center;
    font-size: 2.5em;
    font-weight: 600;

    svg {
      margin-right: 16px;
    }
  }

  button,
  input {
    height: 36px;
    padding: 0 16px;
  }

  input {
    font-size: 16px;
    color: #222f3e;
    border: 2px solid #222f3e;
    border-radius: 4px;

    &::placeholder {
      color: #222f3e;
    }
  }

  a {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 36px;
    background: #10ac84;
    color: #ffffff;
    font-size: 1rem;
    font-weight: 700;
    border-radius: 4px;
    border: 0;
    padding: 0 16px;
    /* width: 100%; */
    margin-right: 16px;
    text-decoration: none;
    transition: background-color 0.2s;

    svg {
      margin-right: 6px;
    }

    &:hover {
      background: ${shade(0.2, '#10ac84')};
    }
  }

  @media screen and (max-width: 767px) {
    flex-direction: column;

    input {
      width: 100%;
      margin-top: 15px;
    }
  }
`;

export const ContentHeaderMenu = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex: 1;

  button {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    margin-right: 16px;
    background: #10ac84;
    color: #ffffff;
    font-weight: 600;
    border: none;
    border-radius: 4px;

    svg {
      margin-right: 6px;
    }

    &:hover {
      background: ${shade(0.2, '#10ac84')};
    }
  }

  @media screen and (max-width: 767px) {
    width: 100%;

    button {
      font-size: 0;
      margin-right: 0;

      svg {
        width: 20px;
        height: 20px;
        margin-right: 0;
      }
    }
  }
`;

export const ContentTable = styled.div`
  margin-bottom: 50px;

  table {
    width: 100%;
    border-collapse: collapse;
    overflow-y: auto;

    thead th {
      text-align: left;
    }

    td,
    th {
      padding: 16px;
    }

    a {
      text-decoration: none;
      font-size: 24px;

      & + a {
        margin-left: 16px;
      }

      &.edit {
        color: #ff9f43;

        &:hover {
          color: ${shade(0.2, '#ff9f43')};
        }
      }

      &.remove {
        color: #ee5253;

        &:hover {
          color: ${shade(0.2, '#ee5253')};
        }
      }
    }

    tbody {
      tr:nth-child(even) {
        background-color: #d3d5d8;
      }

      td:last-child {
        padding: 16px 5px;
      }
    }
  }

  @media screen and (max-width: 380px) {
    width: 320px;
    overflow: auto;
  }
`;
