import React from 'react';

import logo from '../../assets/logo.png';

import { Container, Content, Background } from './styles';

const Login: React.FC = () => (
  <Container>
    <Content>
      <img src={logo} alt="UserFlux" />
      <form>
        <h1>LogIn</h1>

        <input type="text" placeholder="Insira o E-mail" />
        <input type="password" placeholder="Insira a Senha" />

        <button type="submit">LogIn</button>
      </form>
    </Content>
    <Background />
  </Container>
);

export default Login;
