import styled from 'styled-components';
import { shade } from 'polished';

import bgImage from '../../assets/bg-login.png';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  max-width: 490px;

  img {
    width: 100%;
    max-width: 251.7px;
  }

  form {
    margin: 80px 0;
    width: 320px;
    text-align: center;

    h1 {
      font-size: 2.5em;
      margin-bottom: 24px;
    }

    input {
      background: #ffffff;
      border-radius: 4px;
      border: 2px solid #222f3e;
      padding: 16px;
      width: 100%;

      &::placeholder {
        color: #222f3e;
      }

      & + input {
        margin-top: 33px;
      }
    }

    button {
      background: #10ac84;
      color: #ffffff;
      font-size: 1.3em;
      font-weight: 700;
      border-radius: 4px;
      border: 0;
      padding: 16px;
      width: 100%;
      margin-top: 16px;
      transition: background-color 0.2s;

      &:hover {
        background: ${shade(0.2, '#10ac84')};
      }
    }
  }
`;

export const Background = styled.div`
  flex: 1;
  background: url(${bgImage}) no-repeat center;
  background-size: cover;
`;
