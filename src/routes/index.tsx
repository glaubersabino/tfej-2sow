import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from '../pages/Login';
import SignUp from '../pages/SignUp';
import Users from '../pages/Users';

const Routes: React.FC = () => (
  <Switch>
    <Route exact path="/" component={Login} />
    <Route path="/users" component={Users} />
    <Route path="/signup" component={SignUp} />
  </Switch>
);

export default Routes;
